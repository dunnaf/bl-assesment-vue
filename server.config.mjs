/**
 * This is the configuration for nuxt-prestart. You can use nuxt-prestart
 * to perform various tasks before launching the Nuxt server application, such
 * as injecting new environment variables.
 *
 * Read more about @bukalapak/nuxt-prestart here:
 * https://gitlab.cloud.bukalapak.io/buka20/core-frontend/packages/nuxt-packages-monorepo/-/blob/main/packages/nuxt-prestart/README.md
 */

/** @type {import("@bukalapak/nuxt-prestart").NuxtPrestartConfig} */
export default {
  env: {},
}
