export interface IUser {
  id?: number
  username: string
  passwd: string
  type?: string
}
