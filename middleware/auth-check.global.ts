import { whitelistRoutePath } from '~/constants/auth'
import { IUser } from '~/types/global'

export default defineNuxtRouteMiddleware(to => {
  const credential = useCookie<IUser>('user')
  const isRouteWhitelisted = whitelistRoutePath.includes(to.path)
  const loggedIn = Boolean(credential.value)
  if (!isRouteWhitelisted && !loggedIn) return navigateTo('/login')
  if (isRouteWhitelisted && loggedIn) return navigateTo('/')
})
