import { IUser } from '~/types/global'

export const useUserStore = defineStore('users', {
  state: () => ({
    users: [] as IUser[],
    isFetched: false,
  }),
  actions: {
    initUsers(newUsers: IUser[]) {
      this.users = [...newUsers]
      this.isFetched = !this.isFetched
    },
  },
})
