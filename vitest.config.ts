import { defineConfig } from '@bukalapak/test-presets/config/vitest'

export default defineConfig('./.nuxt/tsconfig.json', {
  test: {
    setupFiles: ['./vitest.setup.ts'],
    deps: {
      inline: ['vitest'],
    },
  },
})
