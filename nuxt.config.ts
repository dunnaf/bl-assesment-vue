/**
 * The package `@bukalapak/nuxt` provides a function `withBukalapakConfig`,
 * which will append Bukalapak-specific configuration to your Nuxt app.
 *
 * You can further adjust the configuration to suit your squad's needs:
 * https://v3.nuxtjs.org/api/configuration/nuxt.config
 */
import { defineNuxtConfig, NuxtConfig } from 'nuxt/config'

// TODO(Core FE): publish @bukalapak/nuxt
function withBukalapakConfig(config: NuxtConfig): NuxtConfig {
  return defineNuxtConfig({
    ...config,
    typescript: {
      shim: false,
    },
    runtimeConfig: {
      public: {
        apiBasePath: '',
      },
    },
    modules: [
      '@nuxtjs/tailwindcss',
      [
        '@pinia/nuxt',
        {
          autoImports: ['defineStore'],
        },
      ],
      '@bukalapak/nuxt-request-id',
      [
        // Read more about Bukalapak Nuxt Auth module here:
        // https://bukalapak.atlassian.net/l/cp/AX6QreYm
        '@bukalapak/nuxt-auth',
      ],
      [
        '@bukalapak/nuxt-version-endpoint',
        { version: process.env.APP_VERSION, time: process.env.APP_BUILDTIME },
      ],
      // '@bukalapak/nuxt-sentry',
      '@bukalapak/nuxt-logging',
      [
        '@bukalapak/nuxt-openapi',
        {
          openapiSchema: [],
        },
      ],
      [
        '@pinia/nuxt',
        {
          autoImports: ['defineStore'],
        },
      ],
      ...(config.modules || []),
    ],
  })
}

export default withBukalapakConfig({})
