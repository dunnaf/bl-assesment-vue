## 0.2.0

`2022-10-07`

### Minor changes

- [[9318c35a]](9318c35a193ad40345046680e008c5be0e64f3a2) **added** Add `@bukalapak/nuxt-openapi`
  modules to provide `$openapi` from `@bukalapak/openapi-adapter`.
  - Read more:
    [@bukalapak/openapi-adapter](https://bukalapak.atlassian.net/wiki/spaces/CF/pages/2425163365/bukalapak+openapi-adapter)
- [[9318c35a]](9318c35a193ad40345046680e008c5be0e64f3a2) **added** Add `@bukalapak/stub-request` to
  mock API.
- [[a36b4f7e]](a36b4f7e4604af0bbb6e8c7b46b665860b4ca6d5) **added** Added Vitest as default test
  runner

### Patch changes

- [[6799ff50]](6799ff509d720a706a2f566b536a6595f587af22) **added** Added `nuxt prepare` into the
  postinstall script
- [[ee175536]](ee1755366f34254489ae06674dcc93647abb194b) **changed** Update
  `@bukalapak/nuxt-logging`, `@bukalapak/nuxt-request-id`, `@bukalapak/nuxt-sentry`, and
  `@bukalapak/nuxt-version-endpoint` version
- [[157054aa]](157054aa1fb35ba3353b0839d61594f3274b5f55) **changed** Updated Gitlab CI image to
  support Node 16.15

## 0.1.0

`2022-10-03`

### Minor changes

- **added** Add modules to this template. Modules added:
  [[25f49070]](25f49070d60ba0a941ec2380afb8fa559901ad54)
  - nuxt-sentry
  - nuxt-logging
  - nuxt-request-id
  - nuxt-version-endpoint
- **added** New welcome page [[f0eced97]](f0eced972d14a669cbc2d25062a347ba5f8a2537)
- **added** Synced dependencies and configs with example repo
- **added** Add changelog checking. [[981a7e19]](981a7e1988d60c86055f621a3c722e10ae7b89ec)
