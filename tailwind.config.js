/**
 * The preset `@bukalapak/tailwind-preset` provides a preset configuration that you
 * can use without further configuration.
 *
 * You can further adjust the configuration to suit your squad's needs:
 * https://tailwindcss.com/docs/configuration
 */

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  theme: {
    extend: {},
  },
  plugins: [],
  // TODO(Core FE): Publish @bukalapak/tailwind-preset
  // preset: ['@bukalapak/tailwind-preset'],
}
