import createStubRequest from '@bukalapak/stub-request'
import type { Schema } from '#build/openapi'

const stubRequest = createStubRequest<Schema>()

export default stubRequest
