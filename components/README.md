# Components

More details here: <https://v3.nuxtjs.org/guide/directory-structure/components>

All Vue 3 components are created here. It's advised to use the composition API now, rather than
Options API.
