# {{ project_name }}

{{ description }}

## Quick Start Guide

Initialize

```bash
pnpm install
```

Run local dev server

```bash
pnpm run dev
```

Learn more about how to develop Bukalapak Nuxt app from the
[Confluence Document](https://bukalapak.atlassian.net/wiki/spaces/CF/pages/2355825480/Standalone+-+Bukalapak+Nuxt+App)
