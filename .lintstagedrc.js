/**
 * The shareable configuration `@bukalapak/lint-staged-config` provides some
 * configurations that you can use without further configuration.
 *
 * You can further adjust the configuration to suit your squad's needs:
 * https://github.com/okonet/lint-staged
 */

module.exports = {
  // TODO(Core FE): Publish @bukalapak/lint-staged-config
  // ...require("@bukalapak/lint-staged-config"),
  '*.{js,ts,vue}': ['eslint --fix', 'prettier --write'],
  '*.{css,scss,html,vue}': ['stylelint --fix', 'prettier --write'],
  '*': ['prettier --write --ignore-unknown'],
}
