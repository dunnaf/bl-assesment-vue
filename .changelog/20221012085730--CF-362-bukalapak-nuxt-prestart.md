---
minor: added
---

- Added `@bukalapak/nuxt-prestart` package
- Use `nuxt-prestart` in `start` script
- Add an empty `server.config.mjs` with information about the file
